Meg is a meta language with macros defined by a PEG based syntax and language-independent operations.

# features

  * PEG grammar to define macro syntax's
  * Language-independent operator based operations
  * Pre-processor and ECMAScript libraries
  * Just-in-time (JIT) compiler to execute source
  * JavaScript and C parser generators
  * Generators are re-definable from Meg grammar

# install

Both of these methods require Node 4+ installed on your system.

###### to use as a command line tool

  ```shell
  npm install -g meg
  ```

###### to use as a node.js module

  ```shell
  npm install --save-dev meg
  ```

# license

Copyright (c) 2016+ Futago-za Ryuu<br>
The MIT License, [http://opensource.org/licenses/MIT](http://opensource.org/licenses/MIT)
